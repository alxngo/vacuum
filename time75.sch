EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 8268 11693 portrait
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	6800 1050 6800 1200
Connection ~ 6800 1050
Connection ~ 6500 1050
Wire Wire Line
	6500 1050 6800 1050
Wire Wire Line
	6800 1000 6800 1050
$Comp
L power:+24V #PWR016
U 1 1 602334C9
P 6800 1000
F 0 "#PWR016" H 6800 850 50  0001 C CNN
F 1 "+24V" H 6815 1173 50  0000 C CNN
F 2 "" H 6800 1000 50  0001 C CNN
F 3 "" H 6800 1000 50  0001 C CNN
	1    6800 1000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR017
U 1 1 60232C69
P 6800 1550
F 0 "#PWR017" H 6800 1300 50  0001 C CNN
F 1 "GND" H 6805 1377 50  0001 C CNN
F 2 "" H 6800 1550 50  0001 C CNN
F 3 "" H 6800 1550 50  0001 C CNN
	1    6800 1550
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C4
U 1 1 6022C34B
P 6800 1300
F 0 "C4" H 6708 1254 50  0000 R CNN
F 1 "10uF" H 6708 1345 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 6800 1300 50  0001 C CNN
F 3 "~" H 6800 1300 50  0001 C CNN
	1    6800 1300
	-1   0    0    1   
$EndComp
Wire Wire Line
	6500 1100 6500 1050
Connection ~ 6500 1300
Wire Wire Line
	6400 1300 6500 1300
Wire Wire Line
	6400 1400 6400 1300
Wire Wire Line
	6300 1400 6400 1400
$Comp
L power:GND #PWR015
U 1 1 60227D97
P 6500 1550
F 0 "#PWR015" H 6500 1300 50  0001 C CNN
F 1 "GND" H 6505 1377 50  0001 C CNN
F 2 "" H 6500 1550 50  0001 C CNN
F 3 "" H 6500 1550 50  0001 C CNN
	1    6500 1550
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R6
U 1 1 60225F49
P 6500 1400
F 0 "R6" H 6600 1400 50  0000 C CNN
F 1 "56k" V 6500 1400 25  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 6500 1400 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDA0000/AOA0000C304.pdf" H 6500 1400 50  0001 C CNN
F 4 "PANASONIC" H 6500 1400 50  0001 C CNN "Fabricant"
F 5 "ERJ6ENF5602V" H 6500 1400 50  0001 C CNN "RefFabricant"
F 6 "https://industrial.panasonic.com/ww/products/resistors/chip-resistors/general-purpose-chip-resistors/models/ERJ6ENF5602V" H 6500 1400 50  0001 C CNN "LienFabricant"
F 7 "Farnell" H 6500 1400 50  0001 C CNN "Fournisseur"
F 8 "2303729" H 6500 1400 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2303729" H 6500 1400 50  0001 C CNN "LienFournisseur"
	1    6500 1400
	1    0    0    -1  
$EndComp
Connection ~ 6300 1050
Wire Wire Line
	6300 1050 6100 1050
Wire Wire Line
	6300 1300 6300 1050
$Comp
L power:GND #PWR014
U 1 1 602227BD
P 6300 1550
F 0 "#PWR014" H 6300 1300 50  0001 C CNN
F 1 "GND" H 6305 1377 50  0001 C CNN
F 2 "" H 6300 1550 50  0001 C CNN
F 3 "" H 6300 1550 50  0001 C CNN
	1    6300 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 1050 5900 1050
$Comp
L Device:L_Small L2
U 1 1 60220133
P 6000 1050
F 0 "L2" V 6150 1050 50  0000 C CNN
F 1 "10uH" V 6050 1050 50  0000 C CNN
F 2 "Inductor_SMD:L_Taiyo-Yuden_MD-5050" H 6000 1050 50  0001 C CNN
F 3 "https://ds.yuden.co.jp/TYCOMPAS/eu/specSheet?pn=MDPK5050T100MM" H 6000 1050 50  0001 C CNN
F 4 "Taiyo Yuden" V 6000 1050 50  0001 C CNN "Fabricant"
F 5 "MDPK5050T100MM" V 6000 1050 50  0001 C CNN "RefFabricant"
F 6 "https://ds.yuden.co.jp/TYCOMPAS/eu/detail?pn=MDPK5050T100MM" V 6000 1050 50  0001 C CNN "LienFabricant"
	1    6000 1050
	0    1    -1   0   
$EndComp
Connection ~ 5600 1300
Wire Wire Line
	5600 1300 5600 1050
$Comp
L power:GND #PWR013
U 1 1 6021BFFE
P 5100 1550
F 0 "#PWR013" H 5100 1300 50  0001 C CNN
F 1 "GND" H 5105 1377 50  0001 C CNN
F 2 "" H 5100 1550 50  0001 C CNN
F 3 "" H 5100 1550 50  0001 C CNN
	1    5100 1550
	1    0    0    -1  
$EndComp
Connection ~ 5100 1300
Wire Wire Line
	5700 1300 5600 1300
$Comp
L power:+3V3 #PWR012
U 1 1 602192E3
P 5100 1000
F 0 "#PWR012" H 5100 850 50  0001 C CNN
F 1 "+3V3" H 5115 1173 50  0000 C CNN
F 2 "" H 5100 1000 50  0001 C CNN
F 3 "" H 5100 1000 50  0001 C CNN
	1    5100 1000
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3
U 1 1 60213988
P 5100 1400
F 0 "C3" H 5008 1354 50  0000 R CNN
F 1 "10uF" H 5008 1445 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 5100 1400 50  0001 C CNN
F 3 "~" H 5100 1400 50  0001 C CNN
	1    5100 1400
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R4
U 1 1 6020D1D3
P 5600 1500
F 0 "R4" V 5700 1500 50  0000 C BNN
F 1 "10k" V 5600 1500 25  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 5600 1500 50  0001 C CNN
F 3 "~" H 5600 1500 50  0001 C CNN
	1    5600 1500
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R5
U 1 1 6020A5FC
P 6500 1200
F 0 "R5" H 6600 1200 50  0000 C CNN
F 1 "1.05M" V 6500 1200 25  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 6500 1200 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDA0000/AOA0000C304.pdf" H 6500 1200 50  0001 C CNN
F 4 "PANASONIC" H 6500 1200 50  0001 C CNN "Fabricant"
F 5 "ERJ6ENF1054V" H 6500 1200 50  0001 C CNN "RefFabricant"
F 6 "https://industrial.panasonic.com/ww/products/resistors/chip-resistors/general-purpose-chip-resistors/models/ERJ6ENF1054V" H 6500 1200 50  0001 C CNN "LienFabricant"
F 7 "Farnell" H 6500 1200 50  0001 C CNN "Fournisseur"
F 8 "2303882" H 6500 1200 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2303882" H 6500 1200 50  0001 C CNN "LienFournisseur"
	1    6500 1200
	1    0    0    -1  
$EndComp
Text Notes 4750 2250 0    50   ~ 0
Step 3: Circuit Design & PCB Layout\nhttps://www.instructables.com/Vacuum-Fluorescent-Display-Watch#step3
$Comp
L Device:D_Schottky_Small D1
U 1 1 60224155
P 6400 1050
F 0 "D1" H 6400 843 50  0000 C CNN
F 1 "40V/1A" H 6400 934 50  0000 C CNN
F 2 "Diode_SMD:D_SMA_Handsoldering" V 6400 1050 50  0001 C CNN
F 3 "https://www.onsemi.com/pub/Collateral/MBRA140T3-D.PDF" V 6400 1050 50  0001 C CNN
F 4 "ON SEMICONDUCTOR" H 6400 1050 50  0001 C CNN "Fabricant"
F 5 "MBRA140T3G" H 6400 1050 50  0001 C CNN "RefFabricant"
F 6 "https://www.onsemi.com/products/discretes-drivers/diodes-rectifiers/schottky-diodes-schottky-rectifiers/nrvba140t3g" H 6400 1050 50  0001 C CNN "LienFabricant"
F 7 "AliExpress" H 6400 1050 50  0001 C CNN "Fournisseur"
F 8 "1005001525459843" H 6400 1050 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.aliexpress.com/item/1005001525459843.html" H 6400 1050 50  0001 C CNN "LienFournisseur"
	1    6400 1050
	-1   0    0    1   
$EndComp
Text Notes 5150 1900 0    100  ~ 0
3.3V => 24V@100mA
$Comp
L power:+BATT #PWR04
U 1 1 6030458E
P 2050 1000
F 0 "#PWR04" H 2050 850 50  0001 C CNN
F 1 "+BATT" H 2065 1173 50  0000 C CNN
F 2 "" H 2050 1000 50  0001 C CNN
F 3 "" H 2050 1000 50  0001 C CNN
	1    2050 1000
	1    0    0    -1  
$EndComp
Text Notes 1800 1900 0    100  ~ 0
{2.7~~5.5}V => 3.3V@500mA
$Comp
L Device:R_Small R3
U 1 1 6022BDE4
P 3450 1400
F 0 "R3" H 3550 1400 50  0000 C CNN
F 1 "402k" V 3450 1400 25  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 3450 1400 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDA0000/AOA0000C304.pdf" H 3450 1400 50  0001 C CNN
F 4 "PANASONIC" H 3450 1400 50  0001 C CNN "Fabricant"
F 5 "ERJ6ENF4023V" H 3450 1400 50  0001 C CNN "RefFabricant"
F 6 "https://industrial.panasonic.com/ww/products/resistors/chip-resistors/general-purpose-chip-resistors/models/ERJ6ENF4023V" H 3450 1400 50  0001 C CNN "LienFabricant"
F 7 "Farnell" H 3450 1400 50  0001 C CNN "Fournisseur"
F 8 "2303830" H 3450 1400 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2303830" H 3450 1400 50  0001 C CNN "LienFournisseur"
	1    3450 1400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R2
U 1 1 6022BDF0
P 3450 1200
F 0 "R2" H 3550 1200 50  0000 C CNN
F 1 "200k" V 3450 1200 25  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 3450 1200 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDA0000/AOA0000C304.pdf" H 3450 1200 50  0001 C CNN
F 4 "PANASONIC" H 3450 1200 50  0001 C CNN "Fabricant"
F 5 "ERJ6ENF2003V" H 3450 1200 50  0001 C CNN "RefFabricant"
F 6 "https://industrial.panasonic.com/ww/products/resistors/chip-resistors/general-purpose-chip-resistors/models/ERJ6ENF2003V" H 3450 1200 50  0001 C CNN "LienFabricant"
F 7 "Farnell" H 3450 1200 50  0001 C CNN "Fournisseur"
F 8 "2057656" H 3450 1200 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2057656" H 3450 1200 50  0001 C CNN "LienFournisseur"
	1    3450 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:L_Small L1
U 1 1 6023C210
P 3350 1050
F 0 "L1" V 3500 1050 50  0000 C CNN
F 1 "4.7uH" V 3400 1050 50  0000 C CNN
F 2 "Inductor_SMD:L_Taiyo-Yuden_MD-5050" H 3350 1050 50  0001 C CNN
F 3 "" H 3350 1050 50  0001 C CNN
F 4 "" V 3350 1050 50  0001 C CNN "Fabricant"
F 5 "" V 3350 1050 50  0001 C CNN "RefFabricant"
	1    3350 1050
	0    1    -1   0   
$EndComp
Wire Wire Line
	2450 1500 2400 1500
$Comp
L Device:R_Small R1
U 1 1 6023E706
P 2550 1500
F 0 "R1" V 2650 1500 50  0000 C BNN
F 1 "499k" V 2550 1500 25  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 2550 1500 50  0001 C CNN
F 3 "~" H 2550 1500 50  0001 C CNN
	1    2550 1500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 1500 2400 1300
$Comp
L power:GND #PWR05
U 1 1 602493E5
P 2050 1550
F 0 "#PWR05" H 2050 1300 50  0001 C CNN
F 1 "GND" H 2055 1377 50  0001 C CNN
F 2 "" H 2050 1550 50  0001 C CNN
F 3 "" H 2050 1550 50  0001 C CNN
	1    2050 1550
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C1
U 1 1 602493EC
P 2050 1400
F 0 "C1" H 1958 1354 50  0000 R CNN
F 1 "4.7uF" H 1958 1445 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 2050 1400 50  0001 C CNN
F 3 "~" H 2050 1400 50  0001 C CNN
	1    2050 1400
	-1   0    0    1   
$EndComp
Connection ~ 2400 1300
Wire Wire Line
	2400 1300 2050 1300
Wire Wire Line
	3450 1050 3750 1050
Wire Wire Line
	3750 1000 3750 1050
$Comp
L Device:C_Small C2
U 1 1 6026EA41
P 3750 1300
F 0 "C2" H 3658 1254 50  0000 R CNN
F 1 "4.7uF" H 3658 1345 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 3750 1300 50  0001 C CNN
F 3 "~" H 3750 1300 50  0001 C CNN
	1    3750 1300
	-1   0    0    1   
$EndComp
$Comp
L power:+3V3 #PWR08
U 1 1 60270BDA
P 3750 1000
F 0 "#PWR08" H 3750 850 50  0001 C CNN
F 1 "+3V3" H 3765 1173 50  0000 C CNN
F 2 "" H 3750 1000 50  0001 C CNN
F 3 "" H 3750 1000 50  0001 C CNN
	1    3750 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 1300 3250 1050
Wire Wire Line
	3350 1300 3450 1300
Wire Wire Line
	3350 1400 3350 1300
Wire Wire Line
	3250 1400 3350 1400
Connection ~ 3450 1300
Wire Wire Line
	3750 1050 3750 1200
Wire Wire Line
	3450 1100 3450 1050
Connection ~ 3450 1050
Connection ~ 2050 1300
Text Notes 1650 2250 0    50   ~ 0
MCP1603Buck Converter Evaluation BoardUser’s Guide\nhttps://ww1.microchip.com/downloads/en/DeviceDoc/51652a.pdf#G6.627089
Connection ~ 3750 1050
$Comp
L power:+BATT #PWR01
U 1 1 60304230
P 1200 1000
F 0 "#PWR01" H 1200 850 50  0001 C CNN
F 1 "+BATT" H 1215 1173 50  0000 C CNN
F 2 "" H 1200 1000 50  0001 C CNN
F 3 "" H 1200 1000 50  0001 C CNN
	1    1200 1000
	1    0    0    -1  
$EndComp
$Comp
L power:-BATT #PWR02
U 1 1 603065BB
P 1200 1550
F 0 "#PWR02" H 1200 1400 50  0001 C CNN
F 1 "-BATT" H 1200 1700 50  0000 C CNN
F 2 "" H 1200 1550 50  0001 C CNN
F 3 "" H 1200 1550 50  0001 C CNN
	1    1200 1550
	-1   0    0    1   
$EndComp
$Comp
L Device:Battery_Cell BT1
U 1 1 603072AB
P 1200 1350
F 0 "BT1" H 1318 1400 50  0000 L CNN
F 1 "Battery_Cell" H 1318 1355 50  0001 L CNN
F 2 "time75:NCR-18650B" V 1200 1410 50  0001 C CNN
F 3 "https://www.keyelco.com/product-pdf.cfm?p=826" V 1200 1410 50  0001 C CNN
F 4 "KeyStone" H 1200 1350 50  0001 C CNN "Fabricant"
F 5 "54" H 1200 1350 50  0001 C CNN "RefFabricant"
F 6 "https://www.keyelco.com/product.cfm/product_id/826" H 1200 1350 50  0001 C CNN "LienFabricant"
F 7 "Farnell" H 1200 1350 50  0001 C CNN "Fournisseur"
F 8 "1888422" H 1200 1350 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=1888422" H 1200 1350 50  0001 C CNN "LienFournisseur"
	1    1200 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 1300 2650 1300
$Comp
L power:GND #PWR09
U 1 1 60294E0F
P 3750 1550
F 0 "#PWR09" H 3750 1300 50  0001 C CNN
F 1 "GND" H 3755 1377 50  0001 C CNN
F 2 "" H 3750 1550 50  0001 C CNN
F 3 "" H 3750 1550 50  0001 C CNN
	1    3750 1550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 602354EB
P 3250 1550
F 0 "#PWR06" H 3250 1300 50  0001 C CNN
F 1 "GND" H 3255 1377 50  0001 C CNN
F 2 "" H 3250 1550 50  0001 C CNN
F 3 "" H 3250 1550 50  0001 C CNN
	1    3250 1550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 6022BDD8
P 3450 1550
F 0 "#PWR07" H 3450 1300 50  0001 C CNN
F 1 "GND" H 3455 1377 50  0001 C CNN
F 2 "" H 3450 1550 50  0001 C CNN
F 3 "" H 3450 1550 50  0001 C CNN
	1    3450 1550
	1    0    0    -1  
$EndComp
$Comp
L time75:MicroChip-MCP1603 U1
U 1 1 60209415
P 2950 1400
F 0 "U1" H 2950 1650 50  0000 C CNN
F 1 "MCP1603" H 2950 1150 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5_HandSoldering" H 2950 1000 50  0001 L CNN
F 3 "https://ww1.microchip.com/downloads/en/DeviceDoc/22042B.pdf" H 2950 1400 50  0001 L CNN
F 4 "MicroChip" H 2950 900 50  0001 L CNN "Fabricant"
F 5 "MCP1603T-330I/OS" H 2950 800 50  0001 L CNN "RefFabricant"
F 6 "https://www.microchip.com/wwwproducts/en/MCP1603" H 2950 700 50  0001 L CNN "LienFabricant"
F 7 "AliExpress" H 2950 600 50  0001 L CNN "Fournisseur"
F 8 "1005001268643177" H 2950 500 50  0001 L CNN "RefFournisseur"
F 9 "https://fr.aliexpress.com/item/1005001268643177.html" H 2950 400 50  0001 L CNN "LienFournisseur"
	1    2950 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 1500 2050 1550
Wire Wire Line
	3250 1500 3250 1550
Wire Wire Line
	3450 1500 3450 1550
Wire Wire Line
	3750 1400 3750 1550
Wire Wire Line
	5100 1500 5100 1550
Wire Wire Line
	6300 1500 6300 1550
Wire Wire Line
	6500 1500 6500 1550
Wire Wire Line
	6800 1400 6800 1550
Text Notes 550  7550 0    50   ~ 0
Of IVL2-7/5s\nhttps://hackaday.io/project/7805-vfduino/log/37180-of-ivl2-75s\nThe ChronodeVFD Wristwatch\nhttps://hackaday.com/2014/10/26/the-chronodevfd-wristwatch
$Comp
L time75:MAXIM_MAX6920 U6
U 1 1 603B0A55
P 1300 6300
F 0 "U6" H 1300 7100 50  0000 C CNN
F 1 "MAXIM_MAX6920" H 1300 5500 50  0000 C CNN
F 2 "Package_SO:SOIC-20W_7.5x12.8mm_P1.27mm" H 1300 5350 50  0001 L CNN
F 3 "http://www.farnell.com/datasheets/2002188.pdf" H 1300 6250 50  0001 L CNN
F 4 "Maxim integrated products" H 1300 5250 50  0001 L CNN "Fabricant"
F 5 "MAX6920AWP+" H 1300 5150 50  0001 L CNN "RefFabricant"
F 6 "Farnell" H 1300 4950 50  0001 L CNN "Fournisseur"
F 7 "2516938" H 1300 4850 50  0001 L CNN "RefFournisseur"
F 8 "https://fr.farnell.com/w/search?st=2516938" H 1300 4750 50  0001 L CNN "LienFournisseur"
	1    1300 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 1450 1200 1550
$Comp
L power:-BATT #PWR0101
U 1 1 6025B515
P 1200 1850
F 0 "#PWR0101" H 1200 1700 50  0001 C CNN
F 1 "-BATT" H 1300 2000 50  0001 R BNN
F 2 "" H 1200 1850 50  0001 C CNN
F 3 "" H 1200 1850 50  0001 C CNN
	1    1200 1850
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0102
U 1 1 602B77A9
P 850 3050
F 0 "#PWR0102" H 850 2900 50  0001 C CNN
F 1 "+3V3" H 865 3223 50  0000 C CNN
F 2 "" H 850 3050 50  0001 C CNN
F 3 "" H 850 3050 50  0001 C CNN
	1    850  3050
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C5
U 1 1 602CDDD2
P 850 3150
F 0 "C5" H 758 3104 50  0000 R CNN
F 1 "100nF" H 758 3195 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 850 3150 50  0001 C CNN
F 3 "~" H 850 3150 50  0001 C CNN
	1    850  3150
	1    0    0    1   
$EndComp
Wire Wire Line
	900  3050 850  3050
Connection ~ 850  3050
$Comp
L power:GND #PWR0103
U 1 1 602E9C77
P 850 3250
F 0 "#PWR0103" H 850 3000 50  0001 C CNN
F 1 "GND" H 855 3077 50  0001 C CNN
F 2 "" H 850 3250 50  0001 C CNN
F 3 "" H 850 3250 50  0001 C CNN
	1    850  3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 602EC4D3
P 1200 1850
F 0 "#PWR0104" H 1200 1600 50  0001 C CNN
F 1 "GND" H 1205 1677 50  0000 C CNN
F 2 "" H 1200 1850 50  0001 C CNN
F 3 "" H 1200 1850 50  0001 C CNN
	1    1200 1850
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C7
U 1 1 602F52E3
P 2000 3150
F 0 "C7" H 1908 3104 50  0000 R CNN
F 1 "100nF" H 1908 3195 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 2000 3150 50  0001 C CNN
F 3 "~" H 2000 3150 50  0001 C CNN
	1    2000 3150
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 602F52E9
P 2000 3250
F 0 "#PWR0105" H 2000 3000 50  0001 C CNN
F 1 "GND" H 2005 3077 50  0001 C CNN
F 2 "" H 2000 3250 50  0001 C CNN
F 3 "" H 2000 3250 50  0001 C CNN
	1    2000 3250
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0106
U 1 1 6030CB73
P 2000 3050
F 0 "#PWR0106" H 2000 2900 50  0001 C CNN
F 1 "+24V" H 2015 3223 50  0000 C CNN
F 2 "" H 2000 3050 50  0001 C CNN
F 3 "" H 2000 3050 50  0001 C CNN
	1    2000 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 3050 2000 3050
Connection ~ 2000 3050
Text Label 1700 3250 0    50   ~ 0
IVL1
Text Label 1700 3150 0    50   ~ 0
IVL0
Text Label 1700 3350 0    50   ~ 0
IVL2
Text Label 1700 3450 0    50   ~ 0
IVL3
Text Label 1700 3550 0    50   ~ 0
IVL4
Text Label 1700 3650 0    50   ~ 0
IVL5
Text Label 1700 3750 0    50   ~ 0
IVL6
Text Label 1700 3850 0    50   ~ 0
IVL7
Text Label 1700 3950 0    50   ~ 0
IVL8
Text Label 1700 4050 0    50   ~ 0
IVL9
Text Label 1700 4250 0    50   ~ 0
IVL11
Text Label 1700 4350 0    50   ~ 0
IVL12
Text Label 1700 4450 0    50   ~ 0
IVL13
Text Label 1700 5850 0    50   ~ 0
IVL1
Text Label 1700 5750 0    50   ~ 0
IVL0
Text Label 1700 5950 0    50   ~ 0
IVL2
Text Label 1700 6050 0    50   ~ 0
IVL3
Text Label 1700 6150 0    50   ~ 0
IVL4
Text Label 1700 6250 0    50   ~ 0
IVL5
Text Label 1700 6350 0    50   ~ 0
IVL6
Text Label 1700 6450 0    50   ~ 0
IVL7
Text Label 1700 6550 0    50   ~ 0
IVL8
Text Label 1700 6650 0    50   ~ 0
IVL9
Text Label 1700 6850 0    50   ~ 0
IVL11
Text Label 1700 4150 0    50   ~ 0
IVL10
Text Label 1700 6750 0    50   ~ 0
IVL10
$Comp
L power:+3V3 #PWR0108
U 1 1 60426D6C
P 850 5650
F 0 "#PWR0108" H 850 5500 50  0001 C CNN
F 1 "+3V3" H 865 5823 50  0000 C CNN
F 2 "" H 850 5650 50  0001 C CNN
F 3 "" H 850 5650 50  0001 C CNN
	1    850  5650
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C6
U 1 1 60426D72
P 850 5750
F 0 "C6" H 758 5704 50  0000 R CNN
F 1 "100nF" H 758 5795 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 850 5750 50  0001 C CNN
F 3 "~" H 850 5750 50  0001 C CNN
	1    850  5750
	1    0    0    1   
$EndComp
Wire Wire Line
	900  5650 850  5650
Connection ~ 850  5650
$Comp
L power:GND #PWR0109
U 1 1 60426D7A
P 850 5850
F 0 "#PWR0109" H 850 5600 50  0001 C CNN
F 1 "GND" H 855 5677 50  0001 C CNN
F 2 "" H 850 5850 50  0001 C CNN
F 3 "" H 850 5850 50  0001 C CNN
	1    850  5850
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C8
U 1 1 60509B13
P 2000 5750
F 0 "C8" H 1908 5704 50  0000 R CNN
F 1 "100nF" H 1908 5795 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 2000 5750 50  0001 C CNN
F 3 "~" H 2000 5750 50  0001 C CNN
	1    2000 5750
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 60509B19
P 2000 5850
F 0 "#PWR0111" H 2000 5600 50  0001 C CNN
F 1 "GND" H 2005 5677 50  0001 C CNN
F 2 "" H 2000 5850 50  0001 C CNN
F 3 "" H 2000 5850 50  0001 C CNN
	1    2000 5850
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0112
U 1 1 60509B1F
P 2000 5650
F 0 "#PWR0112" H 2000 5500 50  0001 C CNN
F 1 "+24V" H 2015 5823 50  0000 C CNN
F 2 "" H 2000 5650 50  0001 C CNN
F 3 "" H 2000 5650 50  0001 C CNN
	1    2000 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 5650 2000 5650
Connection ~ 2000 5650
$Comp
L power:GND #PWR0113
U 1 1 605115FC
P 900 5150
F 0 "#PWR0113" H 900 4900 50  0001 C CNN
F 1 "GND" H 905 4977 50  0001 C CNN
F 2 "" H 900 5150 50  0001 C CNN
F 3 "" H 900 5150 50  0001 C CNN
	1    900  5150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 60513B2B
P 900 6950
F 0 "#PWR0114" H 900 6700 50  0001 C CNN
F 1 "GND" H 905 6777 50  0001 C CNN
F 2 "" H 900 6950 50  0001 C CNN
F 3 "" H 900 6950 50  0001 C CNN
	1    900  6950
	1    0    0    -1  
$EndComp
$Comp
L time75:MAXIM_MAX6921 U5
U 1 1 603B242C
P 1300 4150
F 0 "U5" H 1300 5400 50  0000 C CNN
F 1 "MAXIM_MAX6921" H 1300 3000 50  0000 C CNN
F 2 "Package_SO:TSSOP-28_4.4x7.8mm_P0.5mm" H 1300 3100 50  0001 L CNN
F 3 "http://www.farnell.com/datasheets/2002189.pdf" H 1300 4200 50  0001 L CNN
F 4 "Maxim integrated products" H 1300 3000 50  0001 L CNN "Fabricant"
F 5 "MAX6921AUI+" H 1300 2900 50  0001 L CNN "RefFabricant"
F 6 "Farnell" H 1300 2700 50  0001 L CNN "Fournisseur"
F 7 "2516936" H 1300 2600 50  0001 L CNN "RefFournisseur"
F 8 "https://fr.farnell.com/w/search?st=2516936" H 1300 2500 50  0001 L CNN "LienFournisseur"
	1    1300 4150
	1    0    0    -1  
$EndComp
Text Label 900  6250 2    50   ~ 0
~SS
Text Label 900  6150 2    50   ~ 0
SCLK
Text Label 900  3450 2    50   ~ 0
MOSI
Text Label 1700 5150 0    50   ~ 0
MISO
Text Label 1700 6950 0    50   ~ 0
MISO
Text Label 900  6050 2    50   ~ 0
MOSI
Text Label 900  3650 2    50   ~ 0
~SS
Text Label 900  3550 2    50   ~ 0
SCLK
Text Label 900  6350 2    50   ~ 0
PWM
Text Label 900  3750 2    50   ~ 0
PWM
$Comp
L time75:MicroChip-MCP1663 U3
U 1 1 6021EB72
P 6000 1400
F 0 "U3" H 6000 1650 50  0000 C CNN
F 1 "MCP1663" H 6000 1150 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5_HandSoldering" H 6000 1000 50  0001 L CNN
F 3 "https://ww1.microchip.com/downloads/en/DeviceDoc/20005406A.pdf" H 6000 1300 50  0001 L CNN
F 4 "MicroChip" H 6000 900 50  0001 L CNN "Fabricant"
F 5 "MCP1663T-E/OT" H 6000 800 50  0001 L CNN "RefFabricant"
F 6 "https://www.microchip.com/wwwproducts/en/MCP1663" H 6000 700 50  0001 L CNN "LienFabricant"
F 7 "AliExpress" H 6000 600 50  0001 L CNN "Fournisseur"
F 8 "4000766066348" H 6000 500 50  0001 L CNN "RefFournisseur"
F 9 "https://fr.aliexpress.com/item/4000766066348.html" H 6000 400 50  0001 L CNN "LienFournisseur"
	1    6000 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 1300 5100 1300
Connection ~ 5450 1300
Wire Wire Line
	5450 1500 5450 1300
Wire Wire Line
	5500 1500 5450 1500
Wire Wire Line
	5600 1300 5450 1300
Text Label 3250 4250 0    50   ~ 0
IVL13
NoConn ~ 3250 3650
NoConn ~ 3250 3850
Text Label 3250 4050 0    50   ~ 0
IVL12
Text Label 3250 3450 0    50   ~ 0
IVL11
Text Label 3250 3250 0    50   ~ 0
IVL10
Text Label 3250 3050 0    50   ~ 0
IVL9
Text Label 2550 3850 2    50   ~ 0
IVL8
Text Label 2550 3750 2    50   ~ 0
IVL7
Text Label 2550 3650 2    50   ~ 0
IVL6
Text Label 2550 3550 2    50   ~ 0
IVL5
Text Label 2550 3450 2    50   ~ 0
IVL4
Text Label 2550 3350 2    50   ~ 0
IVL3
Text Label 2550 3250 2    50   ~ 0
IVL2
Text Label 2550 3050 2    50   ~ 0
IVL0
Text Label 2550 3150 2    50   ~ 0
IVL1
NoConn ~ 3250 3150
NoConn ~ 3250 3350
NoConn ~ 3250 3950
NoConn ~ 3250 4150
NoConn ~ 3250 4350
$Comp
L time75:IVL2-75 U4
U 1 1 601F9936
P 2900 6050
F 0 "U4" H 2900 6600 50  0000 C CNN
F 1 "IVL2-75" H 2900 5500 50  0000 C CNN
F 2 "time75:IVL2-75" H 2900 5400 50  0001 C CNN
F 3 "https://www.gstube.com/data/4228" H 3300 5700 50  0001 C CNN
F 4 "Tube-Store" H 2900 6050 50  0001 C CNN "Fournisseur"
F 5 "https://www.tube-store.shop/products/10pcs-ivl-2-7-5-nixie-vfd-vacuum-fluorescent-display-soviet-tubes-same-date-nos" H 2900 6050 50  0001 C CNN "LienFournisseur"
F 6 "38016968229016" H 2900 6050 50  0001 C CNN "RefFournisseur"
	1    2900 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 3550 3400 3550
Wire Wire Line
	3250 3750 3400 3750
$Comp
L time75:IVL1-75 U7
U 1 1 6021A35B
P 2900 3700
F 0 "U7" H 2900 4500 50  0000 C CNN
F 1 "IVL1-75" H 2900 2900 50  0000 C CNN
F 2 "time75:IVL1-75" H 2900 2700 50  0001 L CNN
F 3 "https://www.gstube.com/data/4952" H 3000 3350 50  0001 C CNN
	1    2900 3700
	1    0    0    -1  
$EndComp
Text Label 2550 6350 2    50   ~ 0
IVL7
Text Label 2550 5650 2    50   ~ 0
IVL0
Text Label 2550 5750 2    50   ~ 0
IVL1
Text Label 2550 5850 2    50   ~ 0
IVL2
Text Label 2550 5950 2    50   ~ 0
IVL3
Text Label 2550 6050 2    50   ~ 0
IVL4
Text Label 2550 6150 2    50   ~ 0
IVL5
Text Label 2550 6250 2    50   ~ 0
IVL6
Text Label 2550 6450 2    50   ~ 0
IVL8
Text Label 3250 5750 0    50   ~ 0
IVL10
Text Label 3250 5950 0    50   ~ 0
IVL11
Text Label 3250 5650 0    50   ~ 0
IVL9
Text Label 3250 6150 0    50   ~ 0
IVL12
Text Label 3250 6250 0    50   ~ 0
IVL13
Wire Wire Line
	3250 5850 3400 5850
Wire Wire Line
	3250 6050 3400 6050
Wire Wire Line
	3400 6050 3500 5950
Wire Wire Line
	3400 5850 3500 5750
Wire Wire Line
	3400 3750 3500 3650
Wire Wire Line
	3400 3550 3500 3450
Wire Wire Line
	3500 5950 3500 5750
Wire Wire Line
	3500 3650 3500 3450
$Comp
L power:GND #PWR023
U 1 1 6040AE07
P 5550 5950
F 0 "#PWR023" H 5550 5700 50  0001 C CNN
F 1 "GND" H 5555 5777 50  0001 C CNN
F 2 "" H 5550 5950 50  0001 C CNN
F 3 "" H 5550 5950 50  0001 C CNN
	1    5550 5950
	1    0    0    -1  
$EndComp
Connection ~ 5550 5550
$Comp
L Switch:SW_Push SW1
U 1 1 6040874D
P 5550 5750
F 0 "SW1" V 5550 5600 50  0000 L CNN
F 1 "RESET" V 5550 5900 50  0000 L CNN
F 2 "" H 5550 5950 50  0001 C CNN
F 3 "~" H 5550 5950 50  0001 C CNN
	1    5550 5750
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR022
U 1 1 60406153
P 5550 5250
F 0 "#PWR022" H 5550 5100 50  0001 C CNN
F 1 "+3V3" H 5565 5423 50  0000 C CNN
F 2 "" H 5550 5250 50  0001 C CNN
F 3 "" H 5550 5250 50  0001 C CNN
	1    5550 5250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 60403AAC
P 5550 5400
F 0 "R9" H 5700 5400 50  0000 C CNN
F 1 "10k" H 5550 5400 25  0000 C CNN
F 2 "" V 5480 5400 50  0001 C CNN
F 3 "~" H 5550 5400 50  0001 C CNN
	1    5550 5400
	-1   0    0    1   
$EndComp
Wire Wire Line
	5550 5550 5350 5550
Text Label 5350 5550 0    50   ~ 0
RST
Wire Wire Line
	5950 3050 5750 3050
Text Label 5750 3050 0    50   ~ 0
RST
Text Label 6150 5550 0    50   ~ 0
LDR
Wire Wire Line
	5650 3050 5450 3050
Wire Wire Line
	5950 3150 5750 3150
Wire Wire Line
	5650 3050 5750 3150
Text Label 5450 3050 0    50   ~ 0
LDR
Text Label 5750 3150 0    50   ~ 0
IO5
$Comp
L Device:R_PHOTO R10
U 1 1 60397E30
P 6350 5400
F 0 "R10" H 6550 5400 50  0000 R CNN
F 1 "LDR" H 6350 5400 25  0000 C CNN
F 2 "" V 6400 5150 50  0001 L CNN
F 3 "~" H 6350 5350 50  0001 C CNN
	1    6350 5400
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR025
U 1 1 603BC1D6
P 6350 5850
F 0 "#PWR025" H 6350 5600 50  0001 C CNN
F 1 "GND" H 6355 5677 50  0001 C CNN
F 2 "" H 6350 5850 50  0001 C CNN
F 3 "" H 6350 5850 50  0001 C CNN
	1    6350 5850
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR024
U 1 1 603B7A50
P 6350 5250
F 0 "#PWR024" H 6350 5100 50  0001 C CNN
F 1 "+3V3" H 6365 5423 50  0000 C CNN
F 2 "" H 6350 5250 50  0001 C CNN
F 3 "" H 6350 5250 50  0001 C CNN
	1    6350 5250
	1    0    0    -1  
$EndComp
Connection ~ 6350 5550
Wire Wire Line
	6350 5550 6150 5550
Connection ~ 5200 3750
Wire Wire Line
	5950 3750 5200 3750
Wire Wire Line
	5750 3650 5950 3650
Wire Wire Line
	5750 3550 5950 3550
Wire Wire Line
	5750 3450 5950 3450
Wire Wire Line
	5750 3350 5950 3350
Wire Wire Line
	5750 3250 5950 3250
Wire Wire Line
	7400 3550 7600 3550
Wire Wire Line
	7300 3650 7400 3550
Wire Wire Line
	5450 3350 5650 3350
Wire Wire Line
	5150 3250 5350 3250
Wire Wire Line
	5350 3250 5450 3350
Wire Wire Line
	5650 3350 5750 3450
Wire Wire Line
	5450 3450 5650 3450
Wire Wire Line
	5150 3350 5350 3350
Wire Wire Line
	5350 3350 5450 3450
Wire Wire Line
	5450 3250 5650 3250
Wire Wire Line
	5150 3150 5350 3150
Wire Wire Line
	5350 3150 5450 3250
Wire Wire Line
	5650 3250 5750 3350
Wire Wire Line
	5650 3450 5750 3550
Wire Wire Line
	5450 3550 5650 3550
Wire Wire Line
	5450 3550 5350 3450
Wire Wire Line
	5750 3650 5650 3550
Wire Wire Line
	5450 3150 5650 3150
Wire Wire Line
	5650 3150 5750 3250
$Comp
L Device:C_Small C9
U 1 1 606EE556
P 5200 3850
F 0 "C9" H 5108 3804 50  0000 R CNN
F 1 "10uF" H 5108 3895 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 5200 3850 50  0001 C CNN
F 3 "~" H 5200 3850 50  0001 C CNN
	1    5200 3850
	-1   0    0    1   
$EndComp
Text Label 5150 3150 0    50   ~ 0
TDO
Text Label 5150 3250 0    50   ~ 0
TMS
Text Label 5150 3350 0    50   ~ 0
TCK
Text Label 5150 3450 0    50   ~ 0
TDI
Wire Wire Line
	5350 3450 5150 3450
$Comp
L power:+3V3 #PWR010
U 1 1 602E43C6
P 5200 3750
F 0 "#PWR010" H 5200 3600 50  0001 C CNN
F 1 "+3V3" H 5215 3923 50  0000 C CNN
F 2 "" H 5200 3750 50  0001 C CNN
F 3 "" H 5200 3750 50  0001 C CNN
	1    5200 3750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 6045A23A
P 5200 3950
F 0 "#PWR0115" H 5200 3700 50  0001 C CNN
F 1 "GND" H 5205 3777 50  0001 C CNN
F 2 "" H 5200 3950 50  0001 C CNN
F 3 "" H 5200 3950 50  0001 C CNN
	1    5200 3950
	1    0    0    -1  
$EndComp
Text Label 7300 3650 2    50   ~ 0
IO0
Text Label 5750 3350 0    50   ~ 0
IO11
Text Label 5750 3650 0    50   ~ 0
IO17
Text Label 5750 3550 0    50   ~ 0
IO14
Text Label 5750 3450 0    50   ~ 0
IO12
Text Label 5750 3250 0    50   ~ 0
IO2
Text Label 5450 3450 0    50   ~ 0
~SS
Text Label 5450 3250 0    50   ~ 0
SCK
Text Label 5450 3550 0    50   ~ 0
MOSI
Text Label 7600 3550 2    50   ~ 0
MISO
Text Label 5450 3150 0    50   ~ 0
PWM
$Comp
L power:GND #PWR011
U 1 1 602EA820
P 6950 3750
F 0 "#PWR011" H 6950 3500 50  0001 C CNN
F 1 "GND" H 6955 3577 50  0001 C CNN
F 2 "" H 6950 3750 50  0001 C CNN
F 3 "" H 6950 3750 50  0001 C CNN
	1    6950 3750
	1    0    0    -1  
$EndComp
Text Notes 5500 4950 0    50   ~ 0
New Part Day: Bouffalo Labs BL602 RISC-V\nWi-Fi/Bluetooth SoC\nhttps://hackaday.com/2020/11/20/new-part-day\n-bouffalo-labs-bl602-risc-v-wi-fi-bluetooth-soc
$Comp
L time75:Pinenut-12S U2
U 1 1 601F7E7D
P 6450 3400
F 0 "U2" H 6450 3900 50  0000 C CNN
F 1 "Pinenut-12S" H 6450 2900 50  0000 C CNN
F 2 "time75:Pinenut-12S" H 6450 2800 50  0001 C CNN
F 3 "https://wiki.pine64.org/wiki/Nutcracker#Pinenut-12S_Module_information" H 6450 3400 50  0001 C CNN
F 4 "Pine64" H 6450 3400 50  0001 C CNN "Fournisseur"
F 5 "https://pine64.com/product/pinenut-model12s-wifi-ble5-stamp" H 6450 3400 50  0001 C CNN "LienFournisseur"
F 6 "12S" H 6450 3400 50  0001 C CNN "RefFournisseur"
	1    6450 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR021
U 1 1 603B6268
P 4400 4400
F 0 "#PWR021" H 4400 4150 50  0001 C CNN
F 1 "GND" H 4405 4227 50  0001 C CNN
F 2 "" H 4400 4400 50  0001 C CNN
F 3 "" H 4400 4400 50  0001 C CNN
	1    4400 4400
	1    0    0    -1  
$EndComp
Text Label 4400 4400 0    50   ~ 0
GND
Text Notes 3450 4950 0    50   ~ 0
Connect BL602 to OpenOCD,\nConnect JTAG Debugger\nhttps://lupyuen.github.io/articles\n/openocd#connect-jtag-debugger-to-pinecone\n
$Comp
L Connector_Generic:Conn_01x05 J1
U 1 1 604A13F7
P 4200 4200
F 0 "J1" H 4200 4500 50  0000 C CNN
F 1 "JTAG" H 4200 3900 50  0000 C CNN
F 2 "" H 4200 4200 50  0001 C CNN
F 3 "~" H 4200 4200 50  0001 C CNN
	1    4200 4200
	-1   0    0    -1  
$EndComp
Text Label 4400 4300 0    50   ~ 0
TDI
Text Label 4400 4200 0    50   ~ 0
TCK
Text Label 4400 4100 0    50   ~ 0
TMS
Text Label 4400 4000 0    50   ~ 0
TDO
Text Notes 3750 3800 0    100  ~ 20
Interface JTAG
Text Notes 6250 2750 0    100  ~ 20
MCU
Text Notes 900  2750 0    100  ~ 20
Driver VFD
$Comp
L Connector_Generic:Conn_01x04 J2
U 1 1 6046E353
P 4200 3250
F 0 "J2" H 4200 2950 50  0000 C CNN
F 1 "UART" H 4200 3450 50  0000 C CNN
F 2 "" H 4200 3250 50  0001 C CNN
F 3 "~" H 4200 3250 50  0001 C CNN
	1    4200 3250
	-1   0    0    1   
$EndComp
Text Label 7300 3150 2    50   ~ 0
UART_RX
Text Label 7300 3050 2    50   ~ 0
UART_TX
Text Label 4400 3250 0    50   ~ 0
UART_RX
Text Label 4400 3150 0    50   ~ 0
UART_TX
$Comp
L power:GND #PWR020
U 1 1 6047BE01
P 4400 3350
F 0 "#PWR020" H 4400 3100 50  0001 C CNN
F 1 "GND" H 4405 3177 50  0001 C CNN
F 2 "" H 4400 3350 50  0001 C CNN
F 3 "" H 4400 3350 50  0001 C CNN
	1    4400 3350
	1    0    0    -1  
$EndComp
Text Label 4400 3350 0    50   ~ 0
GND
$Comp
L power:+3V3 #PWR019
U 1 1 60483366
P 4400 3050
F 0 "#PWR019" H 4400 2900 50  0001 C CNN
F 1 "+3V3" H 4415 3223 50  0000 C CNN
F 2 "" H 4400 3050 50  0001 C CNN
F 3 "" H 4400 3050 50  0001 C CNN
	1    4400 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 3650 7300 3650
Wire Wire Line
	6950 3150 7300 3150
Wire Wire Line
	6950 3050 7300 3050
Text Notes 3750 2750 0    100  ~ 20
Interface UART
Text Notes 2550 750  0    100  ~ 20
Buck 3V3
Text Notes 5650 750  0    100  ~ 20
Boost 24V
Wire Wire Line
	5100 1000 5100 1050
Wire Wire Line
	2050 1000 2050 1050
Wire Wire Line
	1200 1000 1200 1050
Wire Wire Line
	3750 1050 5100 1050
Connection ~ 5100 1050
Wire Wire Line
	5100 1050 5100 1300
Wire Wire Line
	2050 1050 1200 1050
Connection ~ 2050 1050
Wire Wire Line
	2050 1050 2050 1300
Connection ~ 1200 1050
Wire Wire Line
	1200 1050 1200 1150
Text Notes 2500 2750 0    100  ~ 20
IVL-1/75
Wire Notes Line
	500  2400 7750 2400
Text Notes 3500 3500 0    50   ~ 0
2.4@108mA\nR=U/I\n =2.4V/0.108A\n =22ohm
$Comp
L power:+3V3 #PWR03
U 1 1 606E80A8
P 3500 2950
F 0 "#PWR03" H 3500 2800 50  0001 C CNN
F 1 "+3V3" H 3515 3123 50  0000 C CNN
F 2 "" H 3500 2950 50  0001 C CNN
F 3 "" H 3500 2950 50  0001 C CNN
	1    3500 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R7
U 1 1 606FB11B
P 3500 3050
F 0 "R7" H 3600 3050 50  0000 C CNN
F 1 "22" H 3500 3050 26  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 3500 3050 50  0001 C CNN
F 3 "~" H 3500 3050 50  0001 C CNN
	1    3500 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R8
U 1 1 6070DA65
P 3500 5650
F 0 "R8" H 3600 5650 50  0000 C CNN
F 1 "41" H 3500 5650 26  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 3500 5650 50  0001 C CNN
F 3 "~" H 3500 5650 50  0001 C CNN
	1    3500 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 3150 3500 3450
Connection ~ 3500 3450
Text Notes 2500 5350 0    100  ~ 20
IVL-2/75
$Comp
L Device:R R11
U 1 1 6039D60B
P 6350 5700
F 0 "R11" H 6500 5700 50  0000 C CNN
F 1 "10k" H 6350 5700 25  0000 C CNN
F 2 "" V 6280 5700 50  0001 C CNN
F 3 "~" H 6350 5700 50  0001 C CNN
	1    6350 5700
	-1   0    0    1   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 6077D8FE
P 3500 5550
F 0 "#PWR?" H 3500 5400 50  0001 C CNN
F 1 "+3V3" H 3515 5723 50  0000 C CNN
F 2 "" H 3500 5550 50  0001 C CNN
F 3 "" H 3500 5550 50  0001 C CNN
	1    3500 5550
	1    0    0    -1  
$EndComp
Text Notes 3550 6050 0    50   ~ 0
2.4@58mA\nR=U/I\n =2.4V/0.58A\n =41ohm
$EndSCHEMATC
